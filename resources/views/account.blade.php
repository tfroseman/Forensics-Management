@extends('layouts.authed')

@section('content')
    <div class="content">
        <transition>
            <router-view></router-view>
        </transition>
    </div>
@endsection
