require('./bootstrap');
import VueRouter from 'vue-router';
import Vue from 'vue';
import router from './routes';

window.Vue = Vue;
Vue.use(VueRouter);

const app = new Vue({
  el: '#app',
  router
});
