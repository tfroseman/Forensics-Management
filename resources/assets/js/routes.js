import VueRouter from 'vue-router';

const School = require('./views/school.vue');
const Settings = require('./views/settings.vue');
const Staff = require('./views/staff.vue');
const Students = require('./views/students.vue');
const TournamentHome = require('./views/tournament.vue');
const TournamentCreate = require('./views/Tournament/create_tournament.vue');
const TournamentView = require('./views/Tournament/view_tournament.vue');
const RunningTournament = require('./views/Tournament/running_tournament.vue');

const routes = [
  { path: '/school', component: School },
  { path: '/settings', component: Settings },
  { path: '/staff', component: Staff},
  { path: '/students', component: Students},
  { path: '/tournaments', component: TournamentHome },
  { path: '/tournaments/create', component: TournamentCreate },
  { path: '/tournaments/:id', name: 'view_tournament', component: TournamentView },
  { path: '/tournaments/running/:id', name: 'running_tournament', component: RunningTournament }
];


export default new VueRouter({
  routes // short for `routes: routes`
});