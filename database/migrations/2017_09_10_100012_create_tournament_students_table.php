<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentstudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_students', function (Blueprint $table) {
            /**
             * Auto id for the students invited to the tournament
             */
            $table->increments('id');

            /**
             * A unique id that is a combination of school code (unique per tournament) first 4 values
             * Last 4 values are to identify student in school
             * Approx 400 000 school codes and 9 999 student values
             */
            $table->char('masked_id', 8);

            /**
             * The tournament ID
             */
            $table->integer('tournament_id')->unsigned();
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');

            /**
             * The school the student belongs to
             * could gather this from the short code may be an issue
             */
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');


            /**
             * 
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tournament_students');
    }
}
