<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_rooms', function (Blueprint $table) {
            /**
             * Auto id for the room
             */
            $table->increments('id');
            
            /**
             * The tournament this room is active in
             * TODO Remove unused link
             */
            $table->integer('tournament_id')->unsigned();
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');

            /**
             * Points to a room from a list of rooms that the school has open
             */
            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');

            /**
             * Event that is using this room
             */
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('tournament_events')->onDelete('cascade');
            
            /**
             * Default time stamps
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tournament_rooms');
    }
}
