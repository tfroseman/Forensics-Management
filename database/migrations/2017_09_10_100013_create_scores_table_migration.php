<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('scores', function (Blueprint $table) {
            /**
             * Auto id for the score
             */
            $table->increments('id');

            /**
             * Category for which the score was obtained
             * ie 1 would be for Duo Humor
             */
            // $table->integer('category_id')->unsigned();
            // $table->foreign('category_id')->references('id')->on('events');

            /**
             * The round that this score belongs to
             */
            $table->integer('tournament_room_id')->unsigned();
            $table->foreign('tournament_room_id')->references('id')->on('tournament_rooms')->onDelete('cascade');

            /**
             * Student that received this score
             * Points to a student listing that was invited to a tournament
             */
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('tournament_students')->onDelete('cascade');

            /**
             * Score that was received
             */
            $table->smallInteger('score');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('scores');
    }
}
