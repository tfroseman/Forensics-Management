<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            /**
             * Auto id for users
             */
            $table->increments('id');

            /**
             * Full name for the user 
             * ie. Thomas Roseman
             */
            $table->string('name');

            /**
             * Email for the user. Must be unique
             */
            $table->string('email')->unique();

            /**
             * Password for the user
             */
            $table->string('password');

            /**
             * Used to set the account access of the user
             * ie. 0 1 2 3 should'nt need more that this
             * 
             */
            $table->smallInteger('account_level');

            /**
             * After the user is singed in return a token so they dont have to
             * login as fequently
             */
            $table->rememberToken();

            /**
             * 
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
    }
}
