<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {

            /**
             * Auto id for Tournament 
             */
            $table->increments('id');

            /**
             * Title of the tournament
             */
            $table->string('title');

            /**
             * Date that the tournament starts
             */
            $table->date('date');

            /**
             * Number of rounds that the tournament will run for
             * ! Does not count the final round
             */
            $table->smallInteger('number_of_rounds');


            $table->timestamps();
        });

        Schema::table('tournaments', function (Blueprint $table) {
           $table->boolean('started');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('rooms');
        Schema::dropIfExists('tournament_students');
        Schema::dropIfExists('tournaments');
    }
}
