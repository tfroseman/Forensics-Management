<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitedSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invited_schools', function (Blueprint $table) {
            /**
             * Auto id for the invited schools
             */
            $table->increments('id'); 

            /**
             * The tournament the school is invited to
             */
            $table->integer('tournament_id')->unsigned();
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');

            /**
             * Unique Identifer to the tournament a school is invited to
             * Can repeat but not in a single tournament. 
             * Needs to be manualy inforced in software
             */
            $table->char('school_short_code', 4);

            /**
             * The school that is invited
             */
            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');

            /**
             * 
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('invited_schools');
    }
}
