<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            /**
             * 
             */
            $table->increments('id');

            /**
             * Student Name first last
             */
            $table->string('name');

            /**
             * Category in which the student competes in
             * Is a value between 1 and 19 invlusive
             */
            $table->smallInteger('category');

            /**
             * Grade of the student
             * Is a value between 7 and 12 inclusive
             */
            $table->smallInteger('grade');

            /**
             * 
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('students');
    }
}
