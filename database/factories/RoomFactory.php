<?php

use Faker\Generator as Faker;

$factory->define(\App\Room::class, function (Faker $faker) {
    return [
        'name' => $faker->numberBetween(1,700),
        'school_id' => $faker->numberBetween(1,3),
    ];
});
