<?php

use Faker\Generator as Faker;

$factory->define(\App\Round::class, function (Faker $faker) {
    return [
        //
        'round_number' => $faker->numberBetween(1, 5),
        'tournament_id' => $faker->numberBetween(1, 5),
        'category' => $faker->numberBetween(1,19)
    ];
});
