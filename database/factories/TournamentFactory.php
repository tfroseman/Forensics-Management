<?php

use Faker\Generator as Faker;

$factory->define(App\Tournament::class, function (Faker $faker) {
    return [
        //
        'title' => $faker->name(),
        'date' => $faker->date(),
        'number_of_rounds' => 3,
        'school_id' =>  $faker->numberBetween(1,5),
        'started' => false,
    ];
});
