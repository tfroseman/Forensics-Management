<?php

use Faker\Generator as Faker;

$factory->define(\App\InvitedSchool::class, function (Faker $faker) {
    return [
        //
        'tournament_id' => $faker->numberBetween(1,5),
        'school_short_code' => $faker->randomLetter . $faker->randomLetter . $faker->randomLetter . $faker->randomLetter,
        'school_id' => $faker->numberBetween(1,5),
    ];
});
