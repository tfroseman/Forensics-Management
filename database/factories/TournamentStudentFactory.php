<?php

use Faker\Generator as Faker;

$factory->define(\App\TournamentStudent::class, function (Faker $faker) {
    return [
        //
        'masked_id' => $faker->randomLetter . $faker->randomLetter . $faker->randomLetter .
            $faker->randomLetter . $faker->numberBetween(1, 300),

        'tournament_id' => $faker->numberBetween(1,5),
        'student_id' => $faker->numberBetween(1,5),
    ];
});
