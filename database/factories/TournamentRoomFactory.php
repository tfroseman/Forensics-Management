<?php

use Faker\Generator as Faker;

$factory->define(App\TournamentRoom::class, function (Faker $faker) {
    return [
        'tournament_id' => $faker->numberBetween(1,5),
        'room_id' => $faker->numberBetween(1,5),
        'event_id' => $faker->numberBetween(1,19),
    ];
});
