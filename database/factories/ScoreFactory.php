<?php

use Faker\Generator as Faker;

$factory->define(\App\Score::class, function (Faker $faker) {
    return [
        'tournament_room_id' => $faker->numberBetween(1,19),
        'student_id' =>$faker->numberBetween(1,6),
        'score' => $faker->numberBetween(1,4),
    ];
});
