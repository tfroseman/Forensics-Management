<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'grade' => $faker->numberBetween(7,12),
        'category' => $faker->numberBetween(0,12),
        'school_id' => $faker->numberBetween(1,5)
    ];
});
