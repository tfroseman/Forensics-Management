<?php

use Illuminate\Database\Seeder;

class ScoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $room_id = 1;

    	for ($i=0; $i < 7; $i++) { 
            factory(App\Score::class, 4)->create([
                'tournament_room_id' => $room_id,
            ]);
            $room_id = $room_id + 1;
        } 
    }
}
