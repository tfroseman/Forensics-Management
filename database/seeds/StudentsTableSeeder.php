<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create 4 students that belong to school 1
         */
        factory(App\Student::class, 4)->create([
            'category' => 1,
            'school_id' => 1,
        ]);

        /**
         * Create 4 students that belong to school 2
         */
        factory(App\Student::class, 4)->create([
            'category' => 1,
            'school_id' => 2,
        ]);

         /**
         * Create 4 students that belong to school 1
         * Is Category 2
         */
        factory(App\Student::class, 4)->create([
            'category' => 2,
            'school_id' => 1,
        ]);

        /**
         * Create 4 students that belong to school 2
         * Is Category 2
         */
        factory(App\Student::class, 4)->create([
            'category' => 2,
            'school_id' => 2,
        ]);
    }
}
