<?php

use Illuminate\Database\Seeder;

class InvitedSchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('invited_schools')->insert([
            'tournament_id' => 1,
            'school_short_code' => 'aaaa',
            'school_id' => 1,
        ]);

        DB::table('invited_schools')->insert([
            'tournament_id' => 1,
            'school_short_code' => 'bbbb',
            'school_id' => 2,
        ]);
        //factory(App\InvitedSchool::class, 2)->create();
    }
}
