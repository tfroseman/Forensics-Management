<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\User::class, 5)->create();

        DB::table('users')->insert([
            'name' => 'Thomas Roseman',
            'email' => 'test@gmail.com',
            'password' => bcrypt('password'),
            'account_level' => 0,
            'school_id' => 1
        ]);
    }
}