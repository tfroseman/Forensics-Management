<?php

use Illuminate\Database\Seeder;

class TournamentStudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tournament_students')->insert([
            'masked_id' => 'aaaa1111',
            'tournament_id' => 1,
            'student_id' => 1,
        ]);
        DB::table('tournament_students')->insert([
            'masked_id' => 'aaaa2222',
            'tournament_id' => 1,
            'student_id' => 2,
        ]);
        DB::table('tournament_students')->insert([
            'masked_id' => 'aaaa3333',
            'tournament_id' => 1,
            'student_id' => 3,
        ]);
        DB::table('tournament_students')->insert([
            'masked_id' => 'bbbb1111',
            'tournament_id' => 1,
            'student_id' => 4,
        ]);
        DB::table('tournament_students')->insert([
            'masked_id' => 'bbbb2222',
            'tournament_id' => 1,
            'student_id' => 5,
        ]);
        DB::table('tournament_students')->insert([
            'masked_id' => 'bbbb3333',
            'tournament_id' => 1,
            'student_id' => 6,
        ]);
        //factory(\App\TournamentStudent::class, 50)->create();
    }
}
