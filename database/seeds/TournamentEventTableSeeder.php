<?php

use Illuminate\Database\Seeder;

class TournamentEventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [1, 'Extemporaneous Debate', 1]);
        DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [2, 'Extemporaneous Debate', 2]);
        DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [3, 'Extemporaneous Debate', 3]);
        DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [4, 'Extemporaneous Debate', 4]);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [2, 'Congressional Debate (House and Senate)' 2]);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [3, 'Public Forum Debate' 3]);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [4, 'Policy Debate' 4]);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [5, 'Lincoln-Douglas Debate' 5]);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [6, 'United States Extemporaneous Speaking']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [7, 'Storytelling (Middle School)']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [8, 'Prose (Middle School)']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [9, 'Program Oral Interpretation']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [10, 'Poetry (Middle School)']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [11, 'Original Oratory']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [12, 'Mixed Extemporaneous Speaking (Middle School)']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [13, 'International Extemporaneous Speaking']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [14, 'Informative Speaking']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [15, 'Impromptu (Middle School)']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [16, 'Impromptu (Middle School)']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [17, 'Humorous Interpretation']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [18, 'Duo Interpretation']);
        // DB::insert('insert into tournament_events (id, name, round_id) values (?, ?, ?)', [19, 'Dramatic Interpretation']);
    }
}
