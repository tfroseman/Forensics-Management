<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Runs the School seeder which points to SchoolFactory.php
         */
        $this->call(SchoolsTableSeeder::class);

        /**
         * Runs the User seeder which points to UserFactory.php
         */
        $this->call(UsersTableSeeder::class);

        /**
         * Runs the Student seeder which points to StudentFactory.php
         */
        $this->call(StudentsTableSeeder::class);

        /**
         * Runs the TournamentEventSeeder seeder which points to the actual seeder
         */
        $this->call(TournamentEventTableSeeder::class);

        /**
         * Runs the Tournament seeder which points to TournamentFactory.php
         */
        $this->call(TournamentsTableSeeder::class);

        /**
         * Runs the InvitedSchool seeder which points to InvitedSchoolFactory.php
         */
        $this->call(InvitedSchoolTableSeeder::class);

        /**
         * Runs the TouranemtsStudents seeder which points to TournamentStudentsFactory.php
         */
        $this->call(TournamentStudentSeeder::class);

         /**
         * Runs the Room seeder which points to RoomFactory.php
         */
        $this->call(RoomsTableSeeder::class);

        /**
         * Runs the RoundsTableSeeder seeder which points to RoundFactory.php
         */
        $this->call(RoundsTableSeeder::class);

         /**
         * Runs the ScoresTableSeeder seeder which points to ScoresFactory.php
         */
        $this->call(ScoresTableSeeder::class);

        /**
         * 
         */
        $this->call(TournamentRoomTableSeeder::class);
        

    }
}
