<?php

use Illuminate\Database\Seeder;

class RoundsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rounds')->insert([
            'tournament_id' => 1,
            'round_number' => 1,
        ]);

        DB::table('rounds')->insert([
            'tournament_id' => 1,
            'round_number' => 2,
        ]);

        DB::table('rounds')->insert([
            'tournament_id' => 1,
            'round_number' => 3,
        ]);

        DB::table('rounds')->insert([
            'tournament_id' => 1,
            'round_number' => 4,
        ]);
        //factory(App\Round::class, 100)->create();
    }
}
