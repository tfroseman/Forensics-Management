<?php

use Illuminate\Database\Seeder;

class TournamentRoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i=1; $i < 8; $i++) { 
        	factory(App\TournamentRoom::class)->create([
        	'tournament_id' => 1,
        	'event_id' => $i
        ]);
        }
    }
}
