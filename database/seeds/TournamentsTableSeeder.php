<?php

use Illuminate\Database\Seeder;

class TournamentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Tournament::class, 1)->create([
            'school_id' => 1,
            'started' => 1
        ]);
    }
}
