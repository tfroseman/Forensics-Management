<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentStudent extends Model
{
    //

    /**
     * Get the student that was invited
     * @return Collection Student(1)
     */
    public function student() {
        return $this->belongsTo('App\Student');
    }

    /**
     * Get the tournamet for this invited student
     * @return Collection Tournament(1..*)
     */
    public function tournament() {
        return $this->belongsTo('App\Tournament');
    }

    /**
     * Get the scores this invited student received
     * @return Collection Score(1..*)
     */
    public function scores() {
        return $this->hasMany('App\Score');
    }
}
