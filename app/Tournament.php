<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    /**
     * Get the hosting school for the tournament
     * @return Collection School
     */
    public function host_school(){
        return $this->belongsTo('App\School');
    }

    /**
     * Collection of invited schools
     * @return Collection InvitedSchool
     */
    public function invited_schools() {
        return $this->hasMany('App\InvitedSchool');
    }

    /**
     * Collection of Invited Students
     * @return Collection TournamentStudent
     */
    public function invited_students() {
        return $this->hasMany('App\TournamentStudent');
    }

    /**
     * Collection of Rounds
     * @return Collection Round
     */
    public function rounds() {
        return $this->hasMany('App\Round');
    }

    /**
     * Collection of Rooms
     * @return Collection Room
     */
    public function rooms() {
        return $this->hasManyThrough('App\Room', 'App\Round');
    }
}
