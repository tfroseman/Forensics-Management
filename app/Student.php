<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    public function school() {
        return $this->belongsTo('App\School');
    }

    public function tournamentID() {
        return $this->hasMany('App\TournamentStudent');
    }
}
