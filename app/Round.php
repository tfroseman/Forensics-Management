<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    //

    /**
     * Get the tournament the round belongs to
     * @return Collection Tournament
     */
    public function tournament(){
        return $this->belongsTo('App\Tournament');
    }

    /**
     * Get the events hosted this round
     * @return Collection Event
     */
    public function events() {
        return $this->hasMany('App\TournamentEvent');
    }

    /**
     * Get the rooms used in this round
     * @return Collection Room
     */
    public function rooms() {
        return $this->hasManyThrough('App\TournamentEvent', 'App\Room');
    }
}
