<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{

    public function users(){
        return $this->hasMany('App\User');
    }

    public function students(){
        return $this->hasMany('App\Student');
    }

    public function hostedTournaments() {
        return $this->hasMany('App\Tournament', 'school_id');
    }

    public function invitedTo() {
        return $this->hasMany('App\InvitedSchool', 'school_id');
    }


}
