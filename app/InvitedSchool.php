<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvitedSchool extends Model
{
    //

	/**
	 * Tournament a school was invited to
	 * @return Collection Tournament
	 */
    public function invited_to() {
        return $this->belongsTo('App\Tournament');
    }

    /**
     * School that maps to an invite
     * @return Collection School
     */
    public function school() {
        return $this->belongsTo('App\School');
    }
}