<?php

namespace App\Http\Controllers;

use App\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TournamentController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        return $user->school->hostedTournaments->all();
    }

    /**
     *
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // TODO remove function call
        return response()->json(['error' => 'Method Not Allowed'], 405);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO set mass assignable
        $tournament = new Tournament;
        $tournament->title = $request->title;
        $tournament->date = $request->date;
        $tournament->rounds = $request->rounds;
        $tournament->school_id = $request->user()->school->id;

        $tournament->save();

        return $tournament->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // TODO remove function call
        return Tournament::find($id)->with('rounds.events.rooms.scores')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // TODO remove function call
        return response()->json(['error' => 'Method Not Allowed'], 405);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tournament = Tournament::find($id);

        if( $request->started ){
            $tournament->started = $request->started;
        } else {
            $tournament->title = $request->title;
            $tournament->date = $request->date;
            $tournament->rounds = $request->rounds;
        }

        $tournament->save();

        return response()->json(['success' => 'Saved'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // TODO add protection to this endpoint
        Tournament::destroy($id);
        return response()->json(['success' => 'success'], 200);
    }

    /**
     * Start the requested tournament
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function start(Request $request, $id) {
        $tournament = Tournament::find($id);
        $tournament->started = $request->started;
        $tournament->save();

        return response()->json(['success' => 'Started'], 200);
    }
}
