<?php

namespace App\Http\Controllers;

use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SchoolController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get the id of the currently signed in user
        // and fetch the full user model with the matching id
        $id = Auth::id();
        $user = User::find($id);

        $school = $user->school;

        if( policy($school)->view($user, $school) ){

            return response()->json([
                'school'=> $school,
                'student_count' => $school->students->count(),
                'staff_count' => $school->users->count(),
                'tournaments' => $school->invitedTo->count(),
                ]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // Get the id of the currently signed in user
        // and fetch the full user model with the matching id
        $id = Auth::id();
        $user = User::find($id);

        // if the user is an admin of a school send back school details
        if ($user->isAdmin()) {
            return $user->school;
            //return School::find($user->school_id);
        }
        // Send back an empty response of they are not an admin
        return null;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return School::find($id)->delete();
    }
}
