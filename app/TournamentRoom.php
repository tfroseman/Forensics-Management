<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentRoom extends Model
{
    /**
     * [tournament description]
     * @return [type] [description]
     */
    public function tournament() {
        return $this->belongsTo('App\Tournament');
    }

    /**
     * [event description]
     * @return [type] [description]
     */
    public function event() {
    	return $this->belongsTo('App\TournamentEvent', 'event_id');
    }

    /**
     * [scores description]
     * @return [type] [description]
     */
    public function scores() {
    	return $this->hasMany('App\Score');
    }

}
