<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
	/**
	 * Get the room a score was issued in
	 * @return Collection Room
	 */
	public function room()
	{
	    return $this->belongsTo('App\TournamentRoom');
	}

	/**
	 * Get the student that was assigend this score
	 * @return Collection TournamentStudent
	 */
	public function student()
	{
	    return $this->belongsTo('App\TournamentStudent');
	}
}