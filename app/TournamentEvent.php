<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentEvent extends Model
{
    //
    
    /**
     * Get the room this event took place in
     * @return Collection Room
     */
    public function rooms()
    {
        return $this->hasMany('App\TournamentRoom', 'event_id');
    }

    /**
     * Get the round for this event
     * @return Collection Round
     */
    public function round()
    {
        return $this->belongsTo('App\Round');
    }

    /**
     * Get the scores for the event type
     * @return Collection Score
     */
    public function scores()
    {
        return $this->hasManyThrough('App\Score', 'App\Room');
    }
}
