<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/account', 'AccountController@index')->name('account');

Route::resource('/school', 'SchoolController');
Route::resource('/students', 'StudentController');
Route::resource('/staff', 'StaffController');
Route::resource('/room', 'RoomController');
Route::resource('/score', 'ScoreController');
Route::resource('/round', 'RoundController');
Route::resource('/event', 'TournamentEventController');

Route::resource('/tournaments', 'TournamentController');
Route::resource('/invited', 'InvitedSchoolController');

Route::resource('/running', 'StartedTournamentController');


